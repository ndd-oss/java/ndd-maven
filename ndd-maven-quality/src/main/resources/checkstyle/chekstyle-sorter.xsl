<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:strip-space elements="*" />

  <xsl:template match="module">
    <xsl:copy>

      <xsl:attribute name="name">
            <xsl:value-of select="@name" />
        </xsl:attribute>

      <xsl:for-each select="property">
        <xsl:sort select="@name" data-type="text" />
        <xsl:copy>
          <xsl:copy-of select="node() | @*" />
        </xsl:copy>
      </xsl:for-each>

      <xsl:for-each select="module">
        <xsl:sort select="@name" data-type="text" />
        <xsl:apply-templates select="self::node()" />
      </xsl:for-each>

    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
