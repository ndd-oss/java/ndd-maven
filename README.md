
# NDD Maven

Foundations of NDD Maven projects.

The documentation of the master branch is [available on GitLab Pages][ndd-maven-gitlab-pages].

The documentations of the other branches are [available in the GitLab Environments section][ndd-maven-gitlab-environments].
The environments of these branches are accessible in one of the `Available` or `Stopped` tabs then by clicking on the `Open` button at the right.

## Setup

Using Maven:

```xml
<parent>
  <groupId>name.didier.david</groupId>
  <artifactId>ndd-maven-java</artifactId>
  <version>X.Y.Z</version>
</parent>
```

## Reports

Available reports:

- [Maven reports][ndd-maven-maven-reports];
- [Sonar reports][ndd-maven-sonar-reports];

## About

Copyright Ⓒ David DIDIER 2014-2022

<!-- =============================================================================================================== -->

[ndd-maven-gitlab-environments]: https://gitlab.com/ndd-oss/java/ndd-maven/-/environments
[ndd-maven-gitlab-pages]: https://ndd-oss.gitlab.io/java/ndd-maven/
[ndd-maven-maven-reports]: https://ndd-oss.gitlab.io/java/ndd-maven/project-reports.html
[ndd-maven-sonar-reports]: https://sonarcloud.io/project/overview?id=ndd-oss:java:ndd-maven
